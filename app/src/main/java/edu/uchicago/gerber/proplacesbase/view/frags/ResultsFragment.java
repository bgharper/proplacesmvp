package edu.uchicago.gerber.proplacesbase.view.frags;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.presenter.ResultsPresenter;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.interfaces.ResultView;


public class ResultsFragment extends BaseFragment<ResultsPresenter> implements ResultView {

    private static final String RESULT_DATA = "results";
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private YelpResultsData resultsData;

    private interface OnItemSelectedListener {
        void itemSelected(int position);
    }


    public static ResultsFragment newInstance(YelpResultsData resultData) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(RESULT_DATA, resultData);
        ResultsFragment resultsFragment = new ResultsFragment();
        resultsFragment.setArguments(bundle);
        return resultsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_result, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.result_recycler);

        resultsData = (YelpResultsData) getArguments().getSerializable(RESULT_DATA);
        if (resultsData != null) {
            adapter = new RecyclerAdapter(resultsData, new OnItemSelectedListener() {
                @Override
                public void itemSelected(int position) {
                    presenter.saveClickedItem(resultsData.businesses.get(position));
                    //call back to the single Activity that hosts all frags
                    //when .showPlacesFragment() is shown, then onStart(), onResume() and onStop() will be called

                    getNavActivity().showPlacesFragment(PrefsMgr.getString(view.getContext(), PrefsMgr.TYPE));

                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    protected ResultsPresenter createPresenter() {
        return new ResultsPresenter();
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        private ArrayList<String> data;
        private OnItemSelectedListener onItemSelectedListener;


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;

            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.pop_text);
            }
        }

        public RecyclerAdapter(YelpResultsData resultsData, OnItemSelectedListener onItemSelectedListener) {
            data = resultsData.getSimpleValues();
            this.onItemSelectedListener = onItemSelectedListener;
        }

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pop_layout, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.mTextView.setText(data.get(position));
            holder.mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemSelectedListener.itemSelected(position);
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}
