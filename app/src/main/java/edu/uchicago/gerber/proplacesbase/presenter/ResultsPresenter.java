package edu.uchicago.gerber.proplacesbase.presenter;

import java.util.Date;

import edu.uchicago.gerber.proplacesbase.model.Business;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.view.interfaces.ResultView;
import io.realm.Realm;


public class ResultsPresenter extends Presenter<ResultView> {


    public void saveClickedItem(Business business) {

        Place restaurant = new Place(
                0,
                business.name,
                business.location.city,
                business.location.address.get(0),
                business.phone,
                business.url,
                business.image_url,
                business.categories.get(0).get(1),
                new Date().getTime(),
                PlacePresenter.sRESTAURANTS

        );

        Realm.getInstance(getView().provideContext()).beginTransaction();
        Realm.getInstance(getView().provideContext()).copyToRealm(restaurant);
        Realm.getInstance(getView().provideContext()).commitTransaction();

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }


}
