package edu.uchicago.gerber.proplacesbase.view.frags;

import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.presenter.NewPresenter;
import edu.uchicago.gerber.proplacesbase.presenter.PlacePresenter;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.activities.NavActivity;
import edu.uchicago.gerber.proplacesbase.view.interfaces.NewView;


public class NewFragment extends BaseFragment<NewPresenter> implements NewView {
    protected static final int RESULT_SPEECH = 1234;

    private int mItemid = -1; //-1 denotes no item selected

    private EditText mNameField, mCityField;
    private Button mSpeechButton, mExtractButton, mContactsButton;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    boolean mIsListening = false;
    private static float SPEECH_RATE_NORMAL = 0.95f;
    private static float SPEECH_RATE_SLOWER = 0.8f;

    TextToSpeech t1;

    //this is a proxy to our database
    private InputMethodManager mImm;
    private ProgressDialog progressDialog;

    private boolean mArgsPut = false;

    EditText.OnKeyListener enterListener = new EditText.OnKeyListener() {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };


    public static NewFragment newInstance(String text) {
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        if(text!=null) {
            args.putSerializable("SHARE", text.toString());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_scroll_new, container, false);


    //    mNameField = (EditText) v.findViewById(R.id.edit_main_name);
        mCityField = (EditText) v.findViewById(R.id.edit_main_city);

        //each required field must monitor itself and other text field
        mExtractButton = (Button) v.findViewById(R.id.extract_yelp_button);
      //  mSpeechButton = (Button) v.findViewById(R.id.speech_search);
      //  mContactsButton = (Button) v.findViewById(R.id.contacts_import_button);

        mImm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        //listen for enter and save the record
      //  mNameField.setOnKeyListener(enterListener);
        mCityField.setOnKeyListener(enterListener);
        mCityField.setText(PrefsMgr.getString(getContext(), PrefsMgr.CITY));

//        //button behaviors
        mExtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide soft keyboard
                presenter.performSearch(getActivity().getResources().getString(R.string.mcdr), mCityField.getText().toString());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mCityField.getWindowToken(), 0);
               // imm.hideSoftInputFromWindow(mNameField.getWindowToken(), 0);
            }
        });

//        mContactsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectContact();
//            }
//        });

        // Speech stuff
//        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.getContext());
//        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getActivity().getPackageName());
//
//        SpeechRecognitionListener listener = new SpeechRecognitionListener();
//        mSpeechRecognizer.setRecognitionListener(listener);
//
//        mSpeechButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!mIsListening)
//                {
//                    mIsListening = true;
//                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
//                }
//            }
//        });

//        t1 = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//
//            }
//        });
//        t1.setSpeechRate(SPEECH_RATE_NORMAL);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        if(getArguments().getSerializable("SHARE")!=null) {
//            String shared = getArguments().getSerializable("SHARE").toString();
//
//            getArguments().putSerializable("SHARE", null);
//            String sharedarr[] = shared.split("\n", 4);
//            if (sharedarr.length >= 4) {
//                presenter.performSearch(sharedarr[0], sharedarr[1] + " " + sharedarr[2]);
//            } else {
//                Log.d("uush", sharedarr[0]);
//                mNameField.setText(shared);
//            }
//        }

    }
//
    // Select Contact Button
    //

    public void selectContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, Uri.parse("contents://contact"));
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        startActivityForResult(intent, NavActivity.CONTACT_REQUEST_CODE);
    }

    // For shared contacts
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NavActivity.CONTACT_REQUEST_CODE
                && resultCode == NavActivity.RESULT_OK) {
            Uri uri = data.getData();
            addContact(uri);
        }
    }


    public void addContact(Uri uri) {

        String contactName;
        String contactType;
        String contactCity;
        String photo;
        String contactAddress;
        String contactPhone;

        Cursor cursor = getNavActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        long id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        // If there's no thumbnail, there's no image at all--use default
        if(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI)) == null)
            photo = "https://cdn2.hubspot.net/hub/1547213/file-3362151540-jpg/blog-files/questionmark.jpg";

        else photo = photoUri.toString();
        cursor.close();


        cursor = getNavActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ ContactsContract.CommonDataKinds.StructuredPostal.STREET},
                ContactsContract.Data.CONTACT_ID + "=? AND " +
                        ContactsContract.CommonDataKinds.StructuredPostal.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE},
                null);
        cursor.moveToFirst();
        try {
            contactAddress = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.CommonDataKinds.StructuredPostal.STREET));
        } catch (Exception e) {
            contactAddress = "";
        }
        cursor.close();
        cursor = getNavActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ ContactsContract.CommonDataKinds.StructuredPostal.CITY},
                ContactsContract.Data.CONTACT_ID + "=? AND " +
                        ContactsContract.CommonDataKinds.StructuredPostal.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE},
                null);
        cursor.moveToFirst();
        try {
            contactCity = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.CommonDataKinds.StructuredPostal.CITY));
        } catch (Exception e) {
            contactCity = "";
        }
        if(contactCity==null) contactCity = "";

        cursor.close();
        cursor = getNavActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ ContactsContract.CommonDataKinds.Nickname.NAME},
                ContactsContract.Data.CONTACT_ID + "=? AND " +
                        ContactsContract.CommonDataKinds.Nickname.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE},
                null);
        contactType = "contact";
        cursor = getNavActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.Data.CONTACT_ID + "=? AND " +
                        ContactsContract.CommonDataKinds.Phone.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                null);
        cursor.moveToFirst();
        try {
            contactPhone = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.NUMBER));
        } catch (Exception e) {
            contactPhone = "";
        }

        cursor.close();
        Log.d("UUCT", "Name: " + contactName + "nickname: " + contactType + "address: " +
                contactAddress + "phone: " + contactPhone + "photo " + photo);
        presenter.addClickedContact(contactName, contactType, contactAddress,
                contactCity, contactPhone, photo);
        getNavActivity().showPlacesFragment(PlacePresenter.sALL);
    }
    @Override
    protected NewPresenter createPresenter() {
        return new NewPresenter();
    }

    @Override
    public void startProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Fetching data");
        progressDialog.setMessage("One moment please...");
        progressDialog.setCancelable(true);

        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.cancelSearch();
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    public void stopProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void displaySearchResults(YelpResultsData results) {
        //call back to the single Activity that hosts all frags
        //when .showResultFragment() is shown, then onStart(), onResume() and eventually onStop() will be called
        getNavActivity().showResultFragment(results);
    }


    protected class SpeechRecognitionListener implements RecognitionListener {
        private static final java.lang.String DELIMITER = "in ";

        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onBeginningOfSpeech() {
            mCityField.setText("");
        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
            // We set listening to false here in case no "results" are returned
            mIsListening = false;
        }

        @Override
        public void onError(int error) {

        }

        @Override
        public void onResults(Bundle results) {

            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            Log.d("UUSP", matches != null ? matches.get(0) : null);

            String text = matches.get(0);

            if (text.contains(DELIMITER)) {

                String[] speeched = text.split(DELIMITER);

                mNameField.setText(speeched[0]);
                mCityField.setText(speeched[1]);
            }

            else {
                if (mCityField.getText().toString().equals("")){
                    t1.setSpeechRate(SPEECH_RATE_SLOWER);
                    t1.speak("Please say. Place. in. City.", TextToSpeech.QUEUE_FLUSH, null);
                    t1.setSpeechRate(SPEECH_RATE_NORMAL);
                }
            }
            if(!mCityField.getText().toString().equals("")) {
                presenter.performSearch(mNameField.getText().toString(), mCityField.getText().toString());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mCityField.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mNameField.getWindowToken(), 0);
            }

        }

        @Override
        public void onPartialResults(Bundle partialResults) {
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
        }
    }


}

