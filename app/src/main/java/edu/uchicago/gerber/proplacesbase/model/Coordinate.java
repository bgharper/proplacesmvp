package edu.uchicago.gerber.proplacesbase.model;

import java.io.Serializable;


public class Coordinate implements Serializable {

    public String latitude;
    public String longitude;


    public Coordinate(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }



}
