package edu.uchicago.gerber.proplacesbase.view.interfaces;

import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;


public interface NewView extends BaseView {

    void displaySearchResults(YelpResultsData results);
}
