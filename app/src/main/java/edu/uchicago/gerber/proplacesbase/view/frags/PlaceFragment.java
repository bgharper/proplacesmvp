package edu.uchicago.gerber.proplacesbase.view.frags;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.design.widget.FloatingActionButton;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.presenter.PlacePresenter;
import edu.uchicago.gerber.proplacesbase.presenter.adapters.PlaceAdapter;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.interfaces.PlaceView;
import io.realm.RealmResults;


public class PlaceFragment extends BaseFragment<PlacePresenter> implements PlaceView {

    private PlaceAdapter mAdapter;
  //  private Button mButtonSearch, mButtonSpeechSearch;
    private String mPlaceType;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    boolean mIsListening = false;

    public PlaceFragment() {
    }

    public static PlaceFragment newInstance(String placeType) {
        PlaceFragment fragment = new PlaceFragment();
        fragment.mPlaceType = placeType;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_list, container, false);
        PrefsMgr.setString(view.getContext(), PrefsMgr.TYPE, mPlaceType);
       // mButtonSearch = (Button) view.findViewById(R.id.buttonSearch);
      //  mButtonSpeechSearch = (Button) view.findViewById(R.id.buttonSpeechSearch);

//        mButtonSearch.setOnClickListener(new View.OnClickListener() {
//            // TODO: fix search
//            @Override
//            public void onClick(View arg0) {
//
//                // get prompts.xml view
//                LayoutInflater li = LayoutInflater.from(getActivity());
//                View promptsView = li.inflate(R.layout.prompts, null);
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                        getActivity());
//
//                // set prompts.xml to alertdialog builder
//                alertDialogBuilder.setView(promptsView);
//
//                final EditText userInput = (EditText) promptsView
//                        .findViewById(R.id.editTextDialogUserInput);
//
//                // set dialog message
//                alertDialogBuilder
//                        .setCancelable(false)
//                        .setPositiveButton("OK",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog,int id) {
//                                        // get user input and set it to result
//                                        // edit text
//                                        // result.setText(userInput.getText());
//                                        presenter.filterByName(userInput.getText().toString());
//
//                                    }
//                                })
//                        .setNegativeButton("Cancel",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog,int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
//
//            }
//
//        });

//        mButtonSpeechSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!mIsListening)
//                {
//                    mIsListening = true;
//                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
//                }
//            }
//        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call back to the single Activity that hosts all frags
                //when .showEditFragment() is shown, then onStart(), onResume() and eventually onStop() will be called
                getNavActivity().showEditFragment(null);
            }
        });


        mAdapter = new PlaceAdapter(getActivity(), new PlaceAdapter.OnItemClickListener() {
            public void onItemClick(final Place placeClicked) {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                ListView modeList = new ListView(getActivity());

                String[] stringArray = new String[]{
                        "Edit ",  //0
                        "Share ", //1
                        "Map of ", //2
                        "Dial ",  //3
                        "Yelp site ",  //4
                        "Navigate to ",  //5
                        "Delete ", //6
                        "Cancel ",//7
                        placeClicked.getFavorite() == 1 ? "Unfavorite this Place" : "Favorite this Place" //8

                };

                ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
                modeList.setAdapter(modeAdapter);
                builder.setView(modeList);
                final Dialog dialog = builder.create();


                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog.setTitle(placeClicked.getName());
                dialog.show();
                modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        try {
                            switch (position) {
                                case 0:
                                    //edit
                                    //does nothing yet, will need to be implemented.
                                    editPlaceDialog(placeClicked);

                                    break;
                                case 1:
                                    //share
                                    presenter.sharePlace(placeClicked);
                                    break;

                                case 2:
                                    //map of
                                    presenter.mapPlace(placeClicked);
                                    break;
                                case 3:
                                    //dial
                                    presenter.dialPlace(placeClicked);
                                    break;
                                case 4:
                                    //yelp site
                                    presenter.yelpSite(placeClicked);
                                    break;
                                case 5:
                                    //navigate to : needs to be implemented
                                    presenter.navigateTo(placeClicked);
                                    break;
                                //delete
                                case 6:
                                    presenter.deletePlace(placeClicked);
                                    break;
                                case 7:
                                    //cancel
                                    break;

                                //toggle the favorite
                                case 8:
                                    presenter.toggleFavorite(placeClicked);

                                    break;
                            }
                        } catch (Exception e) {
                            //gracefully handle exceptions
                            e.printStackTrace();
                            presenter.showError( e.getMessage());

                        }

                        dialog.dismiss();
                    }
                });

            }
        });

        ListView listView = (ListView) view.findViewById(R.id.place_list_view);
        listView.setAdapter(mAdapter);
        listView.setEmptyView(view.findViewById(R.id.empty_textview));


        // Speech stuff
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.getContext());
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getActivity().getPackageName());

        SpeechRecognitionListener listener = new SpeechRecognitionListener();
        mSpeechRecognizer.setRecognitionListener(listener);

        return view;
    }


    public void editPlaceDialog(final Place place) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.frag_scroll_layout_edit);

        final EditText editName = (EditText) dialog.findViewById(R.id.edit_main_name);
        final EditText editCity = (EditText) dialog.findViewById(R.id.edit_main_city);
        final EditText editAddress = (EditText) dialog.findViewById(R.id.edit_address);
        final EditText editPhone = (EditText) dialog.findViewById(R.id.edit_phone);
        Button submit = (Button) dialog.findViewById(R.id.edit_button_save);
        Button cancel = (Button) dialog.findViewById(R.id.edit_button_cancel);
        final CheckBox checkFav = (CheckBox) dialog.findViewById(R.id.edit_check_favorite);
        final Spinner editType = (Spinner) dialog.findViewById(R.id.edit_category_spinner);
        ArrayAdapter<CharSequence> spinadapt = ArrayAdapter.createFromResource(this.getContext(),
                R.array.category_options, android.R.layout.simple_spinner_item);
        spinadapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editType.setAdapter(spinadapt);
        // set the spinner to our current choice
        int spinpos = spinadapt.getPosition(place.getType());
        editType.setSelection(spinpos);

        editName.setText(place.getName());
        editCity.setText(place.getCity());
        editAddress.setText(place.getAddress());
        editPhone.setText(place.getPhone());

        checkFav.setChecked(place.getFavorite() == 1);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editName.getText().toString();
                String city = editCity.getText().toString();
                String address = editAddress.getText().toString();
                String yelp = place.getYelp();
                String phone = editPhone.getText().toString();
                String type = editType.getSelectedItem().toString();
                int fav = checkFav.isChecked() ? 1 : 0;
                presenter.updatePlace(place, name, city, address, phone, yelp, fav, type);

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected PlacePresenter createPresenter() {
        presenter = new PlacePresenter();
        return presenter;
    }


    //this method is defined in the PlaceView interface and is called from the presenter (PlacesPresenter)
    @Override
    public void displayPlaces(RealmResults<Place> places) {

        mAdapter.setResults(places);
    }
    protected class SpeechRecognitionListener implements RecognitionListener {
        private static final java.lang.String DELIMITER = "in ";

        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onBeginningOfSpeech() {
        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
            // We set listening to false here in case no "results" are returned
            mIsListening = false;
        }

        @Override
        public void onError(int error) {

        }

        @Override
        public void onResults(Bundle results) {
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            String text = matches.get(0);
            if(text!=null && !text.equals("")) {
                presenter.filterByName(text);
            }
        }

        @Override
        public void onPartialResults(Bundle partialResults) {
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
        }
    }

}
