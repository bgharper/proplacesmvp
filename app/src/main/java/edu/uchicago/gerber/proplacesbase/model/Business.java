package edu.uchicago.gerber.proplacesbase.model;

import java.io.Serializable;
import java.util.List;


public class Business implements Serializable {

        public String name = "";
        public String url = "";
        public String image_url = "";
        public String phone = "";
        public Location location;
        public List<List<String>> categories;
        public String rating_img_url = "";

}
