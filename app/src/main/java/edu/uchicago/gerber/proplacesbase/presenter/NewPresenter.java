package edu.uchicago.gerber.proplacesbase.presenter;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Date;

import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.view.interfaces.NewView;
import edu.uchicago.gerber.proplacesbase.yelpapi2.Yelp;
import io.realm.Realm;


public class NewPresenter extends Presenter<NewView> {


    public void addClickedContact(String name,  String nickname,
                                  String address, String city, String phone, String photo) {
        Place place = new Place (
                0,
                name,
                city,
                address,
                phone,
                "",
                photo,
                nickname,
                new Date().getTime(),
                PlacePresenter.sOTHER
        );

        Realm.getInstance(getView().provideContext()).beginTransaction();
        Realm.getInstance(getView().provideContext()).copyToRealm(place);
        Realm.getInstance(getView().provideContext()).commitTransaction();
    }

    private YelpSearchTask yelpSearchTask;

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }


    public void performSearch(String name, String city) {
        if (yelpSearchTask != null) {
            yelpSearchTask.cancel(true);
        }
        yelpSearchTask = new YelpSearchTask();
        yelpSearchTask.execute(name, city);
    }

    public void performSearch(String name) {
        if (yelpSearchTask != null) {
            yelpSearchTask.cancel(true);
        }
        yelpSearchTask = new YelpSearchTask();
        yelpSearchTask.execute(name);
    }

    public void cancelSearch() {
        yelpSearchTask.cancel(true);
        view.stopProgress();
    }

    private class YelpSearchTask extends AsyncTask<String, Void, YelpResultsData> {

        @Override
        protected void onPreExecute() {
            view.startProgress();

        }

        @Override
        protected YelpResultsData doInBackground(String... params) {
            try {

                String name = params[0];
                Yelp yelpApi = new Yelp();
                YelpResultsData yelpSearchResultLocal = null;
                if (params.length > 1) {
                    String location = params[1];
                    yelpSearchResultLocal = yelpApi.searchMultiple(name, location);
                } else {

                    String[] splits = params[0].split("in");
                    yelpSearchResultLocal = yelpApi.searchMultiple(splits[0], splits[1]);
                }
                return yelpSearchResultLocal;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(YelpResultsData yelpResultsData) {
            view.stopProgress();

            if (yelpResultsData == null) {
                view.showToast("No data for that search term");
                return;
            }
            ArrayList<String> stringArrayList = yelpResultsData.getSimpleValues();
            if (stringArrayList == null || stringArrayList.size() == 0) {
                view.showToast("No data for that search term");
                return;
            }

            view.displaySearchResults(yelpResultsData);
        }

    }
}
