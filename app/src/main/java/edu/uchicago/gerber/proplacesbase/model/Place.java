package edu.uchicago.gerber.proplacesbase.model;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Adam Gerber on 5/19/2014.
 * University of Chicago
 */
public class Place extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private int favorite;
    private String name;
    private String city;
    private String address;
    private String phone;
    private String yelp;
    private String imageUrl;
    private String categories;


    private String type;
    private long timestamp;


    public Place() {
    }

    public Place(int favorite, String name, String city, String address, String phone, String yelp,
                 String imageUrl, String categories, long timestamp, String type) {

        this.favorite = favorite;
        this.name = name;
        this.city = city;
        this.address = address;
        this.phone = phone;
        this.yelp = yelp;
        this.imageUrl = imageUrl;
        if(this.imageUrl.equals("")) {
            imageUrl = "https://cdn2.hubspot.net/hub/1547213/file-3362151540-jpg/blog-files/questionmark.jpg";
        }
        this.categories = categories;
        this.timestamp = timestamp;
        this.type= type;

        //use guid here.
        int result = super.hashCode();
        result = 31 * result + favorite;
        if (name != null)
            result = 31 * result + name.hashCode();
        if (city != null)
            result = 31 * result + city.hashCode();
        if (address != null)
            result = 31 * result + address.hashCode();
        if (phone != null)
            result = 31 * result + phone.hashCode();
        if (yelp != null)
            result = 31 * result + yelp.hashCode();
        if (imageUrl != null)
            result = 31 * result + imageUrl.hashCode();


        id = String.valueOf(new UUID((long)result, (long)-result).randomUUID());



    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getYelp() {
        return yelp;
    }

    public void setYelp(String yelp) {
        this.yelp = yelp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
