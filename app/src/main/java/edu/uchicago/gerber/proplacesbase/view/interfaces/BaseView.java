package edu.uchicago.gerber.proplacesbase.view.interfaces;

import android.content.Context;


public interface BaseView {

    void showToast(String message);

    void startProgress();

    void stopProgress();

    Context provideContext();
}
