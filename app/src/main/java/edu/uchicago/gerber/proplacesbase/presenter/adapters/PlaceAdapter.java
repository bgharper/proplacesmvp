package edu.uchicago.gerber.proplacesbase.presenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.presenter.PlacePresenter;

public final class PlaceAdapter extends RealmAdapter<Place, PlaceAdapter.ViewHolder> {

    private Context mContext;

    public static class ViewHolder extends RealmAdapter.ViewHolder {
        public TextView listText;
        public TextView listCity;
        public TextView listAddress;
        public ImageView listNiv;
        public View listTab;
        public LinearLayout listRow;

        public ViewHolder(LinearLayout container) {
            super(container);
            this.listText = (TextView) container.findViewById(R.id.list_text);
            this.listCity = (TextView) container.findViewById(R.id.list_city);
            this.listNiv = (ImageView) container.findViewById(R.id.list_niv);
            this.listAddress = (TextView) container.findViewById(R.id.list_address);
           // this.listType = (TextView) container.findViewById(R.id.list_type);
            this.listTab = container.findViewById(R.id.list_tab);
            this.listRow = (LinearLayout) container.findViewById(R.id.place_entry);
        }
    }

    public interface OnItemClickListener {
         void onItemClick(Place example);
    }

    private final LayoutInflater inflater;
    private final OnItemClickListener listener;

    public PlaceAdapter(Context context, OnItemClickListener listener) {
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.restos_row, parent, false);
        ViewHolder vh = new ViewHolder((LinearLayout) v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final Place item = getItem(position);

        holder.listText.setText(item.getName());
//        holder.listType.setText("Category: " + item.getType());
        holder.listCity.setText("City: " + item.getCity());
        holder.listAddress.setText("Address: " + item.getAddress());
        Glide.with(mContext)
                .load(item.getImageUrl())
                .placeholder(R.drawable.gear_dark)
                .into(holder.listNiv);


        holder.listTab.setBackgroundColor(item.getFavorite() == 0 ?
                holder.listTab.getResources().getColor(R.color.color_restaurant) : mContext.getResources().getColor( R.color.colorAccent));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });
        // handle different colors for different types
        switch (item.getType()) {
            case PlacePresenter.sOTHER:
                holder.listRow.setBackgroundColor(holder.listRow.getResources().getColor(R.color.color_other));
                break;
            case PlacePresenter.sBARS:
                holder.listRow.setBackgroundColor(holder.listRow.getResources().getColor(R.color.color_bar));
                break;
            case PlacePresenter.sRESTAURANTS:
                holder.listRow.setBackgroundColor(holder.listRow.getResources().getColor(R.color.color_restaurant));
                break;
        }
    }

}