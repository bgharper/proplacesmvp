package edu.uchicago.gerber.proplacesbase.model;

import java.io.Serializable;
import java.util.List;


public class Location implements Serializable {

    public Coordinate coordinate;
    public List<String> address;
    public String city;
    public String postal_code;


}
