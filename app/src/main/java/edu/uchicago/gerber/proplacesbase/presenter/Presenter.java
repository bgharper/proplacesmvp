package edu.uchicago.gerber.proplacesbase.presenter;

import android.support.annotation.NonNull;

import edu.uchicago.gerber.proplacesbase.view.interfaces.BaseView;


public abstract class Presenter<View extends BaseView> {

    protected View view;

    public abstract void onStart();

    public abstract void onResume();

    public abstract void onStop();

    public View getView() {
        return view;
    }

    public void setView(@NonNull View vx) {
        this.view = vx;
    }



}
