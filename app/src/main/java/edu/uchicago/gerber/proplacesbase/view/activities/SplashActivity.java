package edu.uchicago.gerber.proplacesbase.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import edu.uchicago.gerber.proplacesbase.R;

/**
 * Created by agerber on 9/12/2016.
 */
public class SplashActivity extends Activity {
    private int SIMUATE_LOADING_TIME = 1500;

    private ImageView loaderImage;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
        );
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);




        //TODO add animation
        // Background code to load data on splash screen
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(SIMUATE_LOADING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(SplashActivity.this, NavActivity.class);
                startActivity(intent);
                finish();


            }
        });
    }

//    @Override
//    public void finish()
//    {
//        super.finish();
//        overridePendingTransition(
//                R.anim.slide_in_right,
//                R.anim.slide_out_left
//        );
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        overridePendingTransition(
//                R.anim.slide_in_right,
//                R.anim.slide_out_left
//        );
//    }
}
