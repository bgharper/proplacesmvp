package edu.uchicago.gerber.proplacesbase.view.frags;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.activities.NavActivity;

/**
 * This is a simple fragment which defines some basic settings--being able to control
 * the sort type (regardless of which type of place is currently being viewed), and the
 * default city in search.
 */


public class SettingsFragment extends Fragment {


    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings, container, false);
        Button applyButton = (Button) view.findViewById(R.id.settings_button_apply);
        final Spinner sortSpinner = (Spinner) view.findViewById(R.id.settings_spinner_sort);
        final EditText cityEdit = (EditText) view.findViewById(R.id.settings_edit_city);

        ArrayAdapter<CharSequence> spinadapt = ArrayAdapter.createFromResource(this.getContext(),
                R.array.sort_options, android.R.layout.simple_spinner_item);
        spinadapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(spinadapt);

        // set the spinner to our current choice
        int spinpos = spinadapt.getPosition(PrefsMgr.getString(view.getContext(), PrefsMgr.SORT));
        sortSpinner.setSelection(spinpos);

        String city;
        if(!(city = PrefsMgr.getString(view.getContext(), PrefsMgr.CITY)).equals(""))
            cityEdit.setText(city);

        applyButton.setOnClickListener(new View.OnClickListener() {

            // Apply the settings if the button is pressed
            @Override
            public void onClick(View v) {
                PrefsMgr.setString(v.getContext(), PrefsMgr.SORT, sortSpinner.getSelectedItem().toString());
                PrefsMgr.setString(v.getContext(), PrefsMgr.CITY, cityEdit.getText().toString());
                ((NavActivity) getActivity()).showPlacesFragment(PrefsMgr.getString(v.getContext(), PrefsMgr.TYPE));
            }
        });
        return view;
    }



}
