package edu.uchicago.gerber.proplacesbase.presenter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.interfaces.PlaceView;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class PlacePresenter extends Presenter<PlaceView> {

    RealmResults<Place> places;
    public static final String sALL = "all";
    public static final String sBARS = "bars";
    public static final String sRESTAURANTS = "restaurants";
    public static final String sOTHER = "other";
    // Values of the sort options in the array resource
    private static final String SORT_TIME = "Time Entered";
    private static final String SORT_FAVS = "Favorited";
    private static final String SORT_TYPE = "Entry Type";
    private static final String SORT_ALPHA = "Alphabetical";

    @Override
    public void onStart() {
        displayPlaces();
    }


    @Override
    public void onResume() {
        displayPlaces();
    }

    private void displayPlaces() {
        String type = PrefsMgr.getString(this.getView().provideContext(), PrefsMgr.TYPE);
        RealmResults<Place> results = filterByCategory(type);


        switch (PrefsMgr.getString(this.getView().provideContext(), PrefsMgr.SORT)) {

            case SORT_TIME:
                places = results.where().findAllSorted("timestamp", Sort.DESCENDING);
                break;
            case SORT_FAVS:
                places = results.where().findAllSorted("favorite", Sort.DESCENDING);
                break;
            case SORT_TYPE:
                places = results.where().findAllSorted("type", Sort.DESCENDING);
                break;
            case SORT_ALPHA:
                places = results.where().findAllSorted("name", Sort.ASCENDING);
                break;
        }
        view.displayPlaces(places);
    }

    @Override
    public void onStop() {


    }

    public void sharePlace(Place placeClicked) {

        String strSubject = "Check out: " + placeClicked.getName();
        String strMessage = "\n\n"; //give the user some room to type a message
        strMessage += "Restaurant: " + placeClicked.getName();
        strMessage += "\nAddress: " + placeClicked.getAddress() + ", " + placeClicked.getCity();
        strMessage += " \n\nPhone: " + PhoneNumberUtils.formatNumber(placeClicked.getPhone());
        strMessage += " \nYelp page: " + placeClicked.getYelp();
        if (placeClicked.getFavorite() == 1) {
            strMessage += "\n[This is one of my favorite restaurants]";
        }
        strMessage += "\n\nsent from ProPlaces";

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, strMessage);
        view.provideContext().startActivity(Intent.createChooser(sharingIntent, "Choose client"));
    }

    public void mapPlace(Place place) {
        Intent intentMapOf = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=" + place.getAddress() + " " + place.getCity()));
        view.provideContext().startActivity(intentMapOf);
    }

    public void dialPlace(Place place) {
        Intent intentDial = new Intent(Intent.ACTION_CALL);
        intentDial.setData(Uri.parse("tel:" + place.getPhone()));
        // view.provideContext().startActivity(intentDial);
    }

    public void yelpSite(Place place) {
        Intent intentYelp = new Intent(Intent.ACTION_VIEW);
        intentYelp.setData(Uri.parse(place.getYelp()));
        view.provideContext().startActivity(intentYelp);
    }

    public void navigateTo(Place place) {

    }

    public void deletePlace(Place place) {
        Realm realm = Realm.getInstance(view.provideContext());
        realm.beginTransaction();
        place = realm.where(Place.class).equalTo("id", place.getId()).findFirst();
        place.removeFromRealm();
        realm.commitTransaction();
        view.displayPlaces(places);
    }

    public void toggleFavorite(Place place) {

        Realm realm = Realm.getInstance(view.provideContext());
        realm.beginTransaction();
        place = realm.where(Place.class).equalTo("id", place.getId()).findFirst();

//        if (place.getFavorite() == 0) {
//            place.setTimestamp(new Date().getTime());
//        }
        place.setFavorite(place.getFavorite() == 0 ? 1 : 0);

        realm.commitTransaction();
        view.displayPlaces(places);

    }

    public void showError(String strErrorMessage){

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.provideContext());
        builder.setTitle("Error")
                .setMessage(strErrorMessage)
                .setCancelable(false);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void filterByName(String string){
        Realm realm = Realm.getInstance(view.provideContext());
        final RealmResults<Place> results = realm.where(Place.class)
                .contains("name", string, Case.INSENSITIVE)
                .findAll();

        view.displayPlaces(results);


    }


    public RealmResults<Place> filterByCategory(String type){


        Realm realm = Realm.getInstance(view.provideContext());
        if (type == null){
            return realm.allObjects(Place.class);
        }


        if(type.equals(PlacePresenter.sALL)) {
            Log.d("UUNAV", "all");
            return realm.allObjects(Place.class);
        }
        Log.d("UUNAV", type);
        final RealmResults<Place> results = realm.where(Place.class)
                .equalTo("type", type)
                .findAll();
        Log.d("UUNAV", String.valueOf(results.size()));
        return results;

    }

    public void updatePlace(Place place, String name, String city, String address,
                            String phone, String yelp, int favorite, String type){
        Realm realm = Realm.getInstance(getView().provideContext());

        realm.beginTransaction();
        place.setName(name);
        place.setCity(city);
        place.setAddress(address);
        place.setPhone(phone);
        place.setYelp(yelp);
        place.setFavorite(favorite);
        place.setType(type);
        realm.commitTransaction();

        view.displayPlaces(realm.allObjects(Place.class));
    }

}
