package edu.uchicago.gerber.proplacesbase.utils;



import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefsMgr {

    private static SharedPreferences sSharedPreferences;
    // for filtering how to display the results
    public static final String TYPE = "type";
    // Store sort preferences
    public static final String SORT = "sort";
    // Default city to text search on
    public static final String CITY = "city";

    public static void setString(Context context, String key, String value) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static String getString(Context context, String key) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return sSharedPreferences.getString(key, null);

    }


    public static void setInt(Context context, String key, int value) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static int getInt(Context context, String key, int defaultValue) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return sSharedPreferences.getInt(key, defaultValue);

    }


    public static void setBoolean(Context context, String key, boolean bVal) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, bVal);
        editor.commit();

    }

    public static boolean getBoolean(Context context, String key, boolean bDefault) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return sSharedPreferences.getBoolean(key, bDefault);

    }

    public static void setBooleanArray(Context context, String key, boolean[] array) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();


        for (int nC = 0; nC < array.length; nC++) {
            editor.putBoolean(key + nC, array[nC]);
        }

        editor.commit();
    }

    public static boolean[] getBooleanArray(Context context, String key, int size) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);

        boolean[] array = new boolean[size];
        for (int nC = 0; nC < array.length; nC++) {
            array[nC] = sSharedPreferences.getBoolean(key + nC, false);
        }
        return array;

    }


}