package edu.uchicago.gerber.proplacesbase.view.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.presenter.PlacePresenter;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.frags.NewFragment;
import edu.uchicago.gerber.proplacesbase.view.frags.PlaceFragment;
import edu.uchicago.gerber.proplacesbase.view.frags.ResultsFragment;
import edu.uchicago.gerber.proplacesbase.view.frags.SettingsFragment;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class NavActivity extends
        AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public static final String VERSION_KEY = "VERSION_KEY";
    //change the version number (to any integer other than the version integer stored in SharedPrefs) to reset the Realm DB
    public static final int VERSION = 2;
    public static final int CONTACT_REQUEST_CODE = 12345;
    private static final int REQUEST_RECORD_AUDIO = 1234;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        checkAudioPermissions();
        // Initialize PrefsMgr if necessary
        if(PrefsMgr.getString(this, PrefsMgr.CITY)==null)
            PrefsMgr.setString(this, PrefsMgr.CITY, "");
        if(PrefsMgr.getString(this, PrefsMgr.SORT)==null)
            PrefsMgr.setString(this, PrefsMgr.SORT, "Time Entered");
        if(PrefsMgr.getString(this, PrefsMgr.TYPE)==null)
            PrefsMgr.setString(this, PrefsMgr.TYPE, PlacePresenter.sALL);

        int storedVersion = PrefsMgr.getInt(this, VERSION_KEY, 0 );
        if (storedVersion == 0 || storedVersion != VERSION){
            resetRealm();
            PrefsMgr.setInt(this, VERSION_KEY, VERSION);
        }

        setContentView(R.layout.activity_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            // create fragment for collection edit buttons
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, PlaceFragment.newInstance(PlacePresenter.sALL), PlaceFragment.class.getName())
                    .commit();

        }
        Intent data = getIntent();
        String action = data.getAction();
        String type = data.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null ) {
            if ("text/plain".equals(type) && data.getStringExtra(Intent.EXTRA_TEXT)!=null) {
                String dataString = data.getStringExtra(Intent.EXTRA_TEXT);
                showEditFragment(dataString);
            }
        }

    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        overridePendingTransition(
//                R.anim.slide_in_right,
//                R.anim.slide_out_left
//        );
//    }

    //only use this if you need to make a serious scheme change
    private void resetRealm() {
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.deleteRealm(realmConfig);
    }

    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void stopProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void showEditFragment(String string) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, NewFragment.newInstance(string));
        ft.addToBackStack(NewFragment.class.getName());
        ft.commit();
    }

    public void showSettingsFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, SettingsFragment.newInstance());
        ft.addToBackStack(SettingsFragment.class.getName());
        ft.commit();
    }

    public void showResultFragment(YelpResultsData resultData) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, ResultsFragment.newInstance(resultData));
        ft.addToBackStack(ResultsFragment.class.getName());
        ft.commit();
    }


    public PlaceFragment showPlacesFragment(String placeType) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        PlaceFragment frag = PlaceFragment.newInstance(placeType);
        ft.replace(R.id.fragment_container, frag);
     //   ft.addToBackStack(PlaceFragment.class.getName());
        ft.commit();
        return frag;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettingsFragment();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void checkAudioPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_all) {
            // Display all places
            showPlacesFragment(PlacePresenter.sALL);

        } else if (id == R.id.nav_bars) {
            showPlacesFragment(PlacePresenter.sBARS);
        } else if (id == R.id.nav_restaurants) {
            showPlacesFragment(PlacePresenter.sRESTAURANTS);
        } else if (id == R.id.nav_other) {
            showPlacesFragment(PlacePresenter.sOTHER);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty. Flag that speech
                // is disabled
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                    boolean speechEnabled = false;
                }
            }
        }
    }


}
