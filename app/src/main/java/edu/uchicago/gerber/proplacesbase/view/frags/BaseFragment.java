package edu.uchicago.gerber.proplacesbase.view.frags;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import edu.uchicago.gerber.proplacesbase.presenter.Presenter;
import edu.uchicago.gerber.proplacesbase.view.activities.NavActivity;
import edu.uchicago.gerber.proplacesbase.view.interfaces.BaseView;


public abstract class BaseFragment<T extends Presenter> extends Fragment implements BaseView {

    protected T presenter;

    protected abstract T createPresenter();


    protected NavActivity getNavActivity() {
        return (NavActivity) getActivity();
    }

    public final T getPresenter() {
        return presenter;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (presenter == null) {
            presenter = createPresenter();

        }
        presenter.setView(this);

    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }


    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void startProgress() {

    }

    @Override
    public void stopProgress() {

    }

    @Override
    public Context provideContext() {
        return this.getContext();
    }

    @Override
    public void showToast(String text) {
        NavActivity navActivity = getNavActivity();
        if (navActivity == null) {
            return;
        }
        navActivity.showToast(text);
    }
}
